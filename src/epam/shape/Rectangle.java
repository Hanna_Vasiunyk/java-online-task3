package epam.shape;

public abstract class Rectangle implements Shape {

    protected int angles;
    protected int square;
    protected String color;

    public Rectangle() {
    }

    public Rectangle(int angles, int square, String color) {
        this.angles = angles;
        this.square = square;
        this.color = color;
    }

    public int getAngles() {
        return angles;
    }

    public void setAngles(int angles) {
        this.angles = angles;
    }

    public int getSquare() {
        return square;
    }

    public void setSquare(int square) {
        this.square = square;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }


    @Override
    public String toString() {
        return "Rectangle{" +
                "angles=" + angles +
                ", square=" + square +
                ", color='" + color + '\'' +
                '}';
    }
}