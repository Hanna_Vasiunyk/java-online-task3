package epam.shape;

import java.util.ArrayList;
import java.util.List;
import java.util.Collections;
public class myMain {
    public static void main(String[] args) {
        List<Rectangle> rectangles = new ArrayList<>();
        List<Circle> circles = new ArrayList<>();
        List<Triangle> triangles = new ArrayList<>();
        BlueRectangle blueRectangle1 = new BlueRectangle(4, 40, "blue");
        BlueRectangle blueRectangle2 = new BlueRectangle(4, 20, "blue");
        BlueRectangle blueRectangle3 = new BlueRectangle(4, 80, "blue");
        BlueRectangle blueRectangle4 = new BlueRectangle(4, 60, "blue");
        BlueRectangle blueRectangle5 = new BlueRectangle(4, 50, "blue");
        YellowRectangle yellowRectangle1 = new YellowRectangle(4, 50, "yellow");
        YellowRectangle yellowRectangle2 = new YellowRectangle(4, 36, "yellow");
        GreenCircle greenCircle1 = new GreenCircle(36, "green");
        GreenCircle greenCircle2 = new GreenCircle(24, "green");
        rectangles.add(blueRectangle1);
        rectangles.add(blueRectangle3);
        rectangles.add(yellowRectangle1);
        circles.add(greenCircle1);
        circles.add(greenCircle2);
        for (Rectangle r:rectangles) {
            r.draw();
        }
        System.out.println("rectangles not sorted");
        System.out.println(rectangles.toString());
        Collections.sort(rectangles, (a, b) -> a.square < b.square ? -1 : a.square == b.square ? 0 : 1);
        System.out.println("Sorted rectangles by square");
        System.out.println(rectangles.toString());
        Collections.sort(rectangles, (a, b) -> a.color.compareToIgnoreCase(b.color));
        System.out.println("Sorted by name");
        System.out.println(rectangles.toString());
        System.out.println("circles not sorted");
        System.out.println(circles.toString());
        Collections.sort(circles, (a, b) -> a.square < b.square ? -1 : a.square == b.square ? 0 : 1);
        System.out.println("Sorted circles by square");
        System.out.println(circles.toString());
        Collections.sort(circles, (a, b) -> a.color.compareToIgnoreCase(b.color));
        System.out.println("Sorted by name");
        System.out.println(circles.toString());
    }
    }