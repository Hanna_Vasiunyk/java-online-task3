package epam.shape;

public class GreenCircle extends Circle {
    @Override
    public void draw() {
        System.out.println("draw green circle");
    }

    public GreenCircle() {
    }

    public GreenCircle(int square, String color) {
        super(square, color);
    }
}
