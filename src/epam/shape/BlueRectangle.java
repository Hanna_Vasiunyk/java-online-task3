package epam.shape;

public class BlueRectangle extends Rectangle {
    @Override
    public void draw() {
        System.out.println("draw blue rectangle");
    }
    public BlueRectangle() {
    }
    public BlueRectangle(int angles, int square, String color) {
        super(angles, square, color);
    }
}


