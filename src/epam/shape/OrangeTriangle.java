package epam.shape;

public class OrangeTriangle extends Triangle {
    @Override
    public void draw() {
        System.out.println("draw orange triangle");
    }

    public OrangeTriangle() {
    }

    public OrangeTriangle(int angles, String square, String color) {
        super(angles, square, color);
    }
}
