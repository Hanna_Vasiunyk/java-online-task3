package epam.shape;

public class YellowRectangle extends Rectangle{
    @Override
    public void draw() {
        System.out.println("draw yellow rectangle");
    }

    public YellowRectangle() {
    }

    public YellowRectangle(int angles, int square, String color) {
        super(angles, square, color);
    }
}
