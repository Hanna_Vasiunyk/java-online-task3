package epam.shape;

public class RedRectangle extends Rectangle {
    @Override
    public void draw() {
        System.out.println("draw red rectangle");
    }

    public RedRectangle() {
    }

    public RedRectangle(int angles, int square, String color) {
        super(angles, square, color);
    }
}
