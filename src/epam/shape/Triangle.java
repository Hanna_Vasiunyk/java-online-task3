package epam.shape;

public  abstract class Triangle implements Shape {
    protected int angles;
    protected String square;
    protected String color;

    public Triangle() {
    }

    public Triangle(int angles, String square, String color) {
        this.angles = angles;
        this.square = square;
        this.color = color;
    }

    public int getAngles() {
        return angles;
    }

    public void setAngles(int angles) {
        this.angles = angles;
    }

    public String getSquare() {
        return square;
    }

    public void setSquare(String square) {
        this.square = square;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
