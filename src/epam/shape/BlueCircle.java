package epam.shape;

public class BlueCircle extends Circle{
    @Override
    public void draw() {
        System.out.println("draw blue circle");
    }
    public BlueCircle() {
        super();
    }
    public BlueCircle(int square, String color) {
        super(square, color);
    }
}

