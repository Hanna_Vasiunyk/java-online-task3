package epam.shape;

public interface Shape {
    public void draw();
}
