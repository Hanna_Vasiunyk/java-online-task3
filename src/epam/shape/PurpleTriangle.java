package epam.shape;

public class PurpleTriangle extends Triangle {
    @Override
    public void draw() {
        System.out.println("draw purple triangle");
    }

    public PurpleTriangle() {
    }

    public PurpleTriangle(int angles, String square, String color) {
        super(angles, square, color);
    }
}
