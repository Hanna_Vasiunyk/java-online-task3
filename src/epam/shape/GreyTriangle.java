package epam.shape;

public class GreyTriangle extends Triangle {
    @Override
    public void draw() {
        System.out.println("draw grey triangle");
    }

    public GreyTriangle() {
    }

    public GreyTriangle(int angles, String square, String color) {
        super(angles, square, color);
    }
}
