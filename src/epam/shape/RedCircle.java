package epam.shape;

public class RedCircle extends Circle {
    @Override
    public void draw() {
        System.out.println("draw red circle");
    }

    public RedCircle() {
    }

    public RedCircle(int square, String color) {
        super(square, color);
    }
}
