package epam.shape;

public abstract class Circle implements Shape {
    protected int square;
    protected String color;

    public Circle() {
    }
    public Circle(int square, String color) {
        this.square = square;
        this.color = color;
    }
    public int getSquare() {
        return square;
    }
    public void setSquare(int square) {
        this.square = square;
    }
    public String getColor() {
        return color;
    }
    public void setColor(String color) {
        this.color = color;
    }
    @Override
    public String toString() {
        return "Circle{" +
                "square=" + square +
                ", color='" + color + '\'' +
                '}';
    }
}
