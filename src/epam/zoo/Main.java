package epam.zoo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Bird> birds = new ArrayList<>();

        Eagle grayEagle = new Eagle(50, "Alan", "gray");
        Parrot greenParrot = new Parrot(10, "Dudu", "green");
        birds.add(greenParrot);
        birds.add(grayEagle);

        System.out.println("Not sorted");
        System.out.println(birds.toString());

        Collections.sort(birds, (a, b) -> a.name.compareToIgnoreCase(b.name));
        System.out.println("Sorted by name");
        System.out.println(birds.toString());

        Collections.sort(birds, (a, b) -> a.width < b.width ? -1 : a.width == b.width ? 0 : 1);
        System.out.println("Sorted by width");
        System.out.println(birds.toString());

        birds.forEach(bird -> {
            System.out.println("I am " + bird.getName() + " and I");
            bird.fly();
            bird.sleep();
            bird.eat();
        });
    }
}
