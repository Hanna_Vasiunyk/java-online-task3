package epam.zoo;

public interface Fish extends Animal {

   public void swim();
}
