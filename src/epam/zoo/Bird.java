package epam.zoo;

public abstract class Bird  implements Animal {

    protected int width;
    protected String name;
    protected String color;

    public Bird(int width, String name, String color) {
        this.width = width;
        this.name = name;
        this.color = color;
    }

    protected void fly () {
        System.out.println("I can fly...");
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Bird{" +
                "width=" + width +
                ", name='" + name + '\'' +
                ", color='" + color + '\'' +
                '}';
    }
}
