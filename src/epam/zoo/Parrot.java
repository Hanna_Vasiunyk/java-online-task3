package epam.zoo;

 class Parrot extends Bird{

     public Parrot(int width, String name, String color) {
         super(width, name, color);
     }

     public void fly() {
         System.out.println("I can fly and say something");
     }
     public void sleep(){
         System.out.println("I sleep 20 h");
     }
     public void eat(){
        System.out.println("Eat Plants");

    }

}
