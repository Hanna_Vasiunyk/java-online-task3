package epam.zoo;

public class Eagle extends Bird {

    public Eagle(int width, String name, String color) {
        super(width, name, color);
    }

    public void fly() {
        System.out.println("I fly and eat you");
    }
    public void sleep(){
        System.out.println("I have never sleep");
    }
    public void eat(){
        System.out.println("I am eagle");

    }
}
